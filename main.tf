variable "aws_secret_key" {}
variable "aws_access_key" {}


provider "aws" {
  region     = "ap-south-1"
  access_key =  "${var.aws_access_key}"
  secret_key =  "${var.aws_secret_key}"
}

resource "aws_subnet" "main1" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "172.31.1.0/24"
  tags {

    Name = "main1"
}
}

resource "aws_instance" "example" {
  ami           = "ami-5b673c34"
  instance_type = "t2.micro"
  key_name = "deployer-key"
  subnet_id = "${aws_subnet.main1.id}"
  associate_public_ip_address = "true"
}
resource "aws_vpc" "main" {
  cidr_block = "172.31.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags {
    Name = "main"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVDswq5T9AXTAzbtga20DPZXa4YH8QvJYAXZi9u8KGbZqh5Z5GL2dVATL4BQ/TJXaILHzhYwY/VJJWQpAQomAVqTue4joLpuedy4f8HWz20DJGtmusYGq42O7Kpwta/S6LCekdhhT3L+WgPLaWKkdhsufHMPThAsuaFKQklzxbbSlKdcsPv1rqYdiiLjsDztbj5v7DR61hZCiGK3OUaC7MN37z8L0s1pey4FbpHyJaXscSls2Jtpfj2mhFxSMtKnFJZDtTBXgpPPBFkOk6Al7dPQqugOiQgOHFiDUV1HOHVd4lkbX3C/tH9FqhAmquezlD2ZyWiVjjcC80xfJg8+r9 root@localhost.localdomain"

}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}


resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
  tags {
      Name = "Public"
  }
  route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.main.id}"
    }
}





resource "aws_security_group" "FrontEnd" {
  name = "FrontEnd"
  tags {
        Name = "FrontEnd"
  }
  description = "ONLY HTTP CONNECTION INBOUD"
  vpc_id = "${aws_vpc.main.id}"

  ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_route_table_association" "PublicAZA" {
    subnet_id = "${aws_subnet.main1.id}"
    route_table_id = "${aws_route_table.public.id}"
}

output "ip" {
  value = "${aws_instance.example.public_ip}"
}


